
package root.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielmunoz
 * Mayo 2021
 * 
 */
@Entity
@Table(name = "pro_proveedores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProProveedores.findAll", query = "SELECT p FROM ProProveedores p"),
    @NamedQuery(name = "ProProveedores.findByProIdProveedor", query = "SELECT p FROM ProProveedores p WHERE p.proIdProveedor = :proIdProveedor"),
    @NamedQuery(name = "ProProveedores.findByProNombre", query = "SELECT p FROM ProProveedores p WHERE p.proNombre = :proNombre"),
    @NamedQuery(name = "ProProveedores.findByProApellido", query = "SELECT p FROM ProProveedores p WHERE p.proApellido = :proApellido"),
    @NamedQuery(name = "ProProveedores.findByProTelefono", query = "SELECT p FROM ProProveedores p WHERE p.proTelefono = :proTelefono"),
    @NamedQuery(name = "ProProveedores.findByProDireccion", query = "SELECT p FROM ProProveedores p WHERE p.proDireccion = :proDireccion")})

public class ProProveedores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "pro_id_proveedor")
    private String proIdProveedor;
    @Size(max = 2147483647)
    @Column(name = "pro_nombre")
    private String proNombre;
    @Size(max = 2147483647)
    @Column(name = "pro_apellido")
    private String proApellido;
    @Size(max = 2147483647)
    @Column(name = "pro_telefono")
    private String proTelefono;
    @Size(max = 2147483647)
    @Column(name = "pro_direccion")
    private String proDireccion;

    public ProProveedores() {
    }

    public ProProveedores(String proIdProveedor) {
        this.proIdProveedor = proIdProveedor;
    }

    public String getProIdProveedor() {
        return proIdProveedor;
    }

    public void setProIdProveedor(String proIdProveedor) {
        this.proIdProveedor = proIdProveedor;
    }

    public String getProNombre() {
        return proNombre;
    }

    public void setProNombre(String proNombre) {
        this.proNombre = proNombre;
    }

    public String getProApellido() {
        return proApellido;
    }

    public void setProApellido(String proApellido) {
        this.proApellido = proApellido;
    }

    public String getProTelefono() {
        return proTelefono;
    }

    public void setProTelefono(String proTelefono) {
        this.proTelefono = proTelefono;
    }

    public String getProDireccion() {
        return proDireccion;
    }

    public void setProDireccion(String proDireccion) {
        this.proDireccion = proDireccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proIdProveedor != null ? proIdProveedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProProveedores)) {
            return false;
        }
        ProProveedores other = (ProProveedores) object;
        if ((this.proIdProveedor == null && other.proIdProveedor != null) || (this.proIdProveedor != null && !this.proIdProveedor.equals(other.proIdProveedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.entity.ProProveedores[ proIdProveedor=" + proIdProveedor + " ]";
    }
    
}
