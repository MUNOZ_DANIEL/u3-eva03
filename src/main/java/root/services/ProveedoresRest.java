
package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.entity.ProProveedores;

/**
 *
 * @author danielmunoz
 * Mayo2021
 */

@Path("/proveedores")
public class ProveedoresRest {
    
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("ProveedoresPU");
    EntityManager em;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response ListarTodo() {
        em = emf.createEntityManager();
        List<AppConfig> lista = em.createNamedQuery("ProProveedores.findAll").getResultList();
        return Response.ok(200).entity(lista).build();
    }
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar) {
        em = emf.createEntityManager();
        ProProveedores proveedor = em.find(ProProveedores.class, idbuscar);
        return Response.ok(200).entity(proveedor).build();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String nuevoProveedor(ProProveedores proveedorNuevo) {
        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(proveedorNuevo);
        em.getTransaction().commit();
        return "Proveedor Guardado";
    }
    @PUT
    public Response actalizarProveedor(ProProveedores proveedorUpdate) {
        em = emf.createEntityManager();
        em.getTransaction().begin();
        proveedorUpdate = em.merge(proveedorUpdate);
        em.getTransaction().commit();
        return Response.ok(proveedorUpdate).build();
    }

    @DELETE
    @Path("/{ideliminar}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    public Response eliminaId(@PathParam("ideliminar") String ideliminar){
        em = emf.createEntityManager();
        em.getTransaction().begin();
        ProProveedores proveedorEliminar = em.getReference(ProProveedores.class, ideliminar);
        em.remove(proveedorEliminar);
        em.getTransaction().commit();
        return Response.ok("Cliente Eliminado").build();
    }
    
}
