
package root.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author danielmunoz
 * Mayo 2021
 */

@ApplicationPath("api")
public class AppConfig extends Application {
    
}