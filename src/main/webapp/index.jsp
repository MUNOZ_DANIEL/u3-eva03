<%-- 
    Document   : index
    Created on : Apr 24, 2021, 4:54:19 PM
    Author     : danielmunoz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            form {
                /* Centrar el formulario en la página */
                margin: 0 auto;
                width: 800px;
                /* Esquema del formulario */
                padding: 1em;
                border: 1px solid #CCC;
                border-radius: 1em;
                background-color: palegoldenrod;
            }

            table, th, td {
                border: 0px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 0px;
            }
            p{
                color: blue;
            }
        </style>
        <title>Home Apps</title>
    </head>
    <h3 style="text-align: center">EVA03-U3 / DEV. APP PROVEEDORES-REST</h3>
    <form name="form">
        <table style="width:100%">
            <thead>
                <tr>
                    <th> A C C E S O S</th>
                    <th>LINK</th>
                </tr>
            </thead>
            <tbody>
                <tr rowspan="2">
                    <td>Bitbucket</td>
                    <td><p>https://DTS_GROUPLabs@bitbucket.org/MUNOZ_DANIEL/u3-eva03.git<br>
                            https://bitbucket.org/MUNOZ_DANIEL/u3-eva03/src/master/</p></td>
                </tr>
                <tr>
                    <td>Heroku</td>
                    <td><p>https://proveedores-db-rest.herokuapp.com</p></td>
                </tr>
                <tr>
                    <td>GET (all DataBase)</td>
                    <td><p>http://localhost:8080/api/proveedores</p></td>
                </tr>
                <tr>
                    <td>GET (for ID)</td>
                    <td><p>http://localhost:8080/api/proveedores/{idbuscar}</p></td>
                </tr>
                <tr>
                    <td>POST</td>
                    <td><p>http://localhost:8080/api/proveedores</p></td>
                </tr>
                <tr>
                    <td>PUT</td>
                    <td><p><a>http://localhost:8080/api/proveedores</a></p></td>
                </tr>
                <tr>
                    <td>DELETE (for ID)</td>
                    <td><p>http://localhost:8080/api/proveedores/{ideliminar}</p></td>
                </tr>
            </tbody>
        </table>
    </form>
    <footer>
        <div style="text-align: center">
            <br />
            <p style="color: black">
                Desarrollado por el alumno DANIEL MUÑOZ PASTENE<br>
                Versión 2.0 con fecha Mayo 2021<br>
            </p><br>
            <img src="LOGO_CIISA_apaisado_PNG.png" width="120" height="70" alt="LOGO_CIISA_apaisado_PNG"/><br>
            <p style="color: black">Taller Aplicaciones Empresariales - IC201IECIREOL | Sección 50 | Evaluación #3 Unidad 3</p>                        
        </div>
    </footer>
</html>
